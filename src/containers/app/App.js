import React, {useEffect, useState} from 'react';
import './App.css';
import Messages from "../../components/messages/messages";
import Send from "../../components/send/send";

function App() {
    const [messages, setMessages] = useState([]);
    const [lastMessage] = useState([{last: ''}]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch('http://146.185.154.90:8000/messages');
            if (response.ok) {
                const messages = await response.json();
                setMessages(messages);
            };
        };
        fetchData();
    }, []);

    if (messages[messages.length - 1] === undefined) {
    } else {
        lastMessage[0].last = messages[messages.length - 1].datetime;
    };

    const getNewMessages = async () => {
        if (lastMessage[0].last === undefined) {
        } else if (lastMessage[0].last.length !== 0) {
            const response = await fetch('http://146.185.154.90:8000/messages?datetime='+lastMessage[0].last+'');
            if (response.ok) {
                const newMessages = await response.json();
                if (newMessages.length !== 0) {
                    if (messages.length ===0 ) {
                    } else {
                        setMessages(messages.concat(newMessages));
                    };
                };
            };
        };
    };
    setInterval(getNewMessages, 3000);

    const data = new URLSearchParams();

    let sendMessage = event => {
        data.set('message', event.target.value);
    };

    let sendAuthor = event => {
        data.set('author', event.target.value);
    };

    let sendPost = async () => {
        await fetch('http://146.185.154.90:8000/messages', {method: 'post', body: data});
    };

    let messagesList = messages.map((messages, index) => {
        return (
            <Messages
                key={index} author={messages.author}
                message={messages.message} dateTime={messages.datetime}
            />
        );
    });

    return (
      <div className="container">
          <div className="scroll">
              {messagesList}
          </div>
          <Send sendMessage={sendMessage} sendAuthor={sendAuthor} send={sendPost}/>
      </div>
  );
}

export default App;
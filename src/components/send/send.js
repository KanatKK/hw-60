import React from 'react';

const Send = props => {
    return (
        <div className="send">
            <input type="text" className="author" placeholder="Author" onChange={props.sendAuthor}/>
            <input type="text" className="message" placeholder="Message" onChange={props.sendMessage}/>
            <button type="button" className="sendBtn" onClick={props.send}>Send</button>
        </div>
    );
};

export default Send;